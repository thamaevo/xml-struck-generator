/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import javax.imageio.ImageIO;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Djaya
 */
public class convertImg {
    public void startConvert(NodeList Listbills, String idPel, String nmPel, String swRef, String jBulan, String jAdmin, String jLunas) {

        for (int temp = 0; temp < Listbills.getLength(); temp++) {
            Node nNode = Listbills.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                //SET UKURAN IMAGE
                int width = 1060;
                int height = 250;

                //CALL CLASS CONVERT
                BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
                Graphics2D g2d = img.createGraphics();
                Font font = new Font("Courier New", Font.PLAIN, 12);
                FontMetrics fm = g2d.getFontMetrics();
                fm = g2d.getFontMetrics();
                g2d = img.createGraphics();

                // SET COLOR BACKGROUD
                g2d.setBackground(Color.WHITE);
                g2d.clearRect(0, 0, width, height);

                // SET COLOR TEXT
                g2d.setColor(Color.BLACK);
                g2d.setFont(font);

                //SET PARAMETER
                String vBulan = eElement.getElementsByTagName("bill_date").item(0).getTextContent();
                String vBillAmount = eElement.getElementsByTagName("bill_amount").item(0).getTextContent();
                String vPenalty = eElement.getElementsByTagName("penalty").item(0).getTextContent();
                String vKubikasi = eElement.getElementsByTagName("kubikasi").item(0).getTextContent();

                //SET CURRENCY
                double drptag = Double.parseDouble(String.valueOf(vBillAmount));
                int dtbayar = parseInt(vBillAmount) + parseInt(jAdmin);
                double Badmin = Double.parseDouble(String.valueOf(jAdmin))/2;
                double tBayar = Double.parseDouble(String.valueOf(dtbayar));;

                //SET MOUNTH AND YEAR
                convertBlnThn getthBln = new convertBlnThn();
                String blnTh = getthBln.findThBulan(vBulan);
                
                //tanggal lunas
                
                String tgl_lunas = jLunas;
                String lunas1 = tgl_lunas.substring(0,4);
                String lunas2 = tgl_lunas.substring(4,6);
                String lunas3 = tgl_lunas.substring(6,8);
                String lunas4 = tgl_lunas.substring(8,10);
                String lunas5 = tgl_lunas.substring(10,12);
                String lunas6 = tgl_lunas.substring(12,14);
                
                //siap cetak
                String[] cetak = new String[17];
                cetak[0] = "BUKTI PEMBAYARAN PDAM";
                cetak[1] = "\n";
                cetak[2] = "NOREK           : "+idPel;
                cetak[3] = "BLN/THN         : "+blnTh;
                cetak[4] = "NAMA            : "+nmPel;
                cetak[5] = "TGL LUNAS       : "+lunas1+"-"+lunas2+"-"+lunas3+" "+lunas4+":"+lunas5+":"+lunas6;
                cetak[6] = "NO REF          : "+swRef;
                cetak[7] = "\n";
                cetak[8] = "RP TAG          : Rp. "+String.format("%,.2f", drptag);
                cetak[9] = "ADMIN           : Rp. "+String.format("%,.2f", Badmin);
                cetak[10] = "TOTAL           : Rp. "+String.format("%,.2f", tBayar);
                cetak[11] = "PDAM Menyatakan struk ini sebagai bukti pembayaran yang sah.";

                //SET VALUE UNTUK SATU BULAN BAGIAN KIRI
                g2d.drawString(cetak[0], 10, 20);
                g2d.drawString(cetak[1], 10, 40);
                g2d.drawString(cetak[2], 10, 60);
                g2d.drawString(cetak[3], 10, 80);
                g2d.drawString(cetak[4], 10, 100);
                g2d.drawString(cetak[5], 10, 120);
                g2d.drawString(cetak[6], 10, 140);
                g2d.drawString(cetak[7], 10, 160);
                g2d.drawString(cetak[8], 10, 180);
                g2d.drawString(cetak[9], 10, 200);
                g2d.drawString(cetak[10], 10, 220);

                //SET VALUE UNTUK SATU BULAN BAGIAN KANAN
                g2d.drawString(cetak[0], 700, 20);
                g2d.drawString(cetak[1], 450, 40);
                g2d.drawString(cetak[2], 450, 60);
                g2d.drawString(cetak[8], 800, 60);
                g2d.drawString(cetak[4], 450, 80);
                g2d.drawString(cetak[5], 450, 100);
                g2d.drawString(cetak[3], 800, 80);
                g2d.drawString(cetak[7], 450, 100);
                g2d.drawString(cetak[6], 450, 120);
                g2d.drawString(cetak[9], 450, 180);
                g2d.drawString(cetak[10], 450, 200);
                g2d.drawString(cetak[11], 550, 160);
                g2d.dispose();

                //SAVE IMAGE
                System.out.println("------------------ LOG SAVE IMAGE " + blnTh + ".png ------------------");
                saveImg(img, idPel+"-"+nmPel.trim()+"-"+blnTh);

                //SET LOG DATA
                System.out.println("PERIODE : " + eElement.getElementsByTagName("bill_date").item(0).getTextContent());
            }
        }

    }
    public void saveImg(BufferedImage img, String Bulan) {
        String status = "";
        try {
            ImageIO.write(img, "png", new File("STRUK_PDAM-" + Bulan + ".png"));
            System.out.println("Simpan gambar berhasil status = SUKSES");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Simpan gambar GAGAL status = GAGAL");
        }
    }
}
